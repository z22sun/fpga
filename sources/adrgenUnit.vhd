
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity adrgenUnit is
    Port ( clk	 		 : in STD_LOGIC;
		   reset	 	 : in STD_LOGIC;
		   I_clr_PtrLine : in STD_LOGIC;
		   I_inc_PtrLine : in STD_LOGIC;
		   I_clr_PtrCol  : in STD_LOGIC;
		   I_inc_PtrCol  : in STD_LOGIC;
		   I_selPix 	 : in STD_LOGIC_VECTOR (1 downto 0);
		   O_EndImage	 : out STD_LOGIC;
		   O_NewLine	 : out STD_LOGIC;
		   O_ADR_R		 : out STD_LOGIC_VECTOR (13 downto 0); -- La profondeur de la mémoire IN = 100x100 = 10000
		   O_ADR_W	 	 : out STD_LOGIC_VECTOR (13 downto 0)  -- La profondeur de la mémoire OUT = 100x100 = 10000   
		   ); 
end adrgenUnit;


architecture Behavioral of adrgenUnit is

-- déclaration des signaux internes
--	_BLANK_

signal PtrLine :unsigned (6 downto 0) := (others => '0');
signal PtrCol :unsigned (6 downto 0) := (others => '0');

signal PtrModifier :unsigned (7 downto 0) := (others => '0');

signal rawPointer :unsigned (13 downto 0) := (others => '0');

begin
    process(clk,reset) -- manages row/col pointers increments and resets
    begin
        if (reset = '1') then 
            PtrLine <= (others => '0');
            PtrCol <= (others => '0');
        elsif (RISING_EDGE(clk)) then
                if (I_clr_PtrLine = '1') then 
                    PtrLine <= (others => '0');
                end if;
                if (I_clr_PtrCol = '1') then
                    PtrCol <= (others => '0');
                end if;
                
                if (I_inc_PtrLine = '1') then
                    if (PtrLine < to_unsigned(97, 7)) then
                        PtrLine <= PtrLine + 1;
                    else 
                        PtrLine <= to_unsigned(0, 7);
                    end if;
                end if;
                
                if (I_inc_PtrCol = '1') then
                    if (PtrCol < to_unsigned(99, 7)) then
                        PtrCol <= PtrCol + 1;
                    else 
                        PtrCol <= to_unsigned(0, 7);
                    end if;
                end if;

        end if;
    end process;
   
    rawPointer <= PtrLine * 100 + PtrCol;

    process(I_selPix) -- manages multiplexer output
    begin
        case (I_selPix) is
            when "00" => 
                 PtrModifier <= to_unsigned(0, 8);
            when "01" => 
                 PtrModifier <= to_unsigned(100, 8);
            when "10" => 
                 PtrModifier <= to_unsigned(200, 8);
            when others => 
                PtrModifier <= to_unsigned(0, 8);
        end case;
    end process;
	
--    process(clk, reset) -- manages raw pointer signal
--    begin
--    if (RISING_EDGE(clk)) then
        
--        rawPointer <= PtrLine * 100 + PtrCol;
        
--        if(reset = '1') then
--            rawPointer <= (others => '0');
--        end if;
--    end if;        
--    end process;
    
    
    O_ADR_R <= STD_LOGIC_VECTOR(rawPointer + ("000000" & PtrModifier)); 
    O_ADR_W <= STD_LOGIC_VECTOR(rawPointer + 99);
    O_EndImage <= '1' when (PtrCol = 99 and PtrLine = 97) else '0';
    O_NewLine <= '1' when (PtrCol = 99) else '0';
    

end Behavioral;

